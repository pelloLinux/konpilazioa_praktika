%error-verbose
%{
   #include <stdio.h>
   #include <iostream>
   #include <vector>
   #include <string>
   #include "Kodea.h"
   #include "Lag.h"
   using namespace std;

   extern int yylex();
   extern int yylineno;
   extern char *yytext;
   void yyerror (const char *msg) {
     printf("line %d: %s at '%s'\n", yylineno, msg, yytext) ;
   }

   expresionstruct * makecomparison(std::string &s1, std::string &s2, std::string &s3) ;
   expresionstruct * makearithmetic(std::string &s1, std::string &s2, std::string &s3) ;
   Kodea kodea;

%}


//Tokenek zein atributu-mota duten

%union {
    string *izena ;
    string *mota ;
    string *par_mota ;
    expresionstruct *adi ;
    IdLista *izenak ;
    int erref ;
    ErrefLista *jarraituBaldin;
}


/*
   Tokenak erazagutu. Honek tokens.l fitxategiarekin
   bat etorri behar du.
*/
%token <izena> TIDENTIFIER TINTEGER TDOUBLE
%token <izena> TMUL TSEMIC TASSIG
%token RPROGRAM RBEGIN REND RVAR RFLOAT RINTEGER RGET RPUT_LINE RIN ROUT RTHEN RCONTINUE RWHILE RDO RIF RPROCEDURE
%token <izena> TCGE TCLE TCLT TCGT TCNE TCEQ TDIV TADD TSUB TCOMMA TLPAREN TRPAREN TCOLON TIN TOUT

// Hemen erazagutu atributuak dauzkaten ez-bukaerakoak. Adibidea:
%type <izena> programa
%type <adi> adierazpena
%type <erref> M
%type <mota> mota
%type <par_mota> par_mota
%type <jarraituBaldin> sententzia sententzia_zerrenda
%type <izenak> parametro_zerrenda parametro_zerrendaren_bestea id_zerrenda id_zerrendaren_bestea


%start programa
%nonassoc TCGE TCLE TCLT TCGT TCNE TCEQ
%left TADD TSUB
%left TMUL TDIV
%%

programa : RPROGRAM
	   TIDENTIFIER { kodea.agGehitu("progama "+*$<izena>2) ; }
	   erazagupenak
	   azpiprogramen_erazagupena
	   RBEGIN
	   sententzia_zerrenda
	   REND TSEMIC{ kodea.agGehitu("halt") ;
			kodea.idatzi() ;}
        ;

erazagupenak : RVAR id_zerrenda TCOLON mota TSEMIC
	{

  		 kodea.erazagupenakGehitu(*$<izenak>2, *$<mota>4);
	}
	erazagupenak | ;

id_zerrenda : TIDENTIFIER
  		id_zerrendaren_bestea
		{
			$<izenak>$ = new IdLista;
			$<izenak>$->push_back(*$<izena>1);
			$<izenak>$->insert($<izenak>$->end(), $<izenak>2->begin(), $<izenak>2->end()) ;
		} ;

id_zerrendaren_bestea : TCOMMA TIDENTIFIER
		 id_zerrendaren_bestea
		{
			$<izenak>$ =  new IdLista;
			$<izenak>$->push_back(*$<izena>2);
			$<izenak>$->insert($<izenak>$->end(), $<izenak>3->begin(), $<izenak>3->end()) ;
		}
		|
		{$<izenak>$ = new IdLista;};

mota : RFLOAT { $<mota>$ = new string; *$<mota>$ = "real"; }
  | RINTEGER { $<mota>$ = new string; *$<mota>$ = "int"; };

azpiprogramen_erazagupena : azpiprogramaren_erazagupena azpiprogramen_erazagupena|;

azpiprogramaren_erazagupena  : RPROCEDURE TIDENTIFIER
				{
					kodea.agGehitu("proc "+ *$<izena>2);
				}
				argumentuak erazagupenak RBEGIN sententzia_zerrenda REND TSEMIC
				{
					kodea.agGehitu("endproc");
				};

argumentuak : TLPAREN parametro_zerrenda TRPAREN|;


parametro_zerrenda : id_zerrenda TCOLON par_mota mota
	{
		kodea.parametroakGehitu(*$<izenak>1, *$<par_mota>3, *$<mota>4);

	} parametro_zerrendaren_bestea;


par_mota : RIN {$<par_mota>$ = new string; *$<par_mota>$ = "in"; }
	|ROUT { $<par_mota>$ = new string; *$<par_mota>$ = "out"; }
	|RIN ROUT { $<par_mota>$ = new string; *$<par_mota>$ = "in out"; };




parametro_zerrendaren_bestea : TSEMIC id_zerrenda TCOLON par_mota mota
	{
		kodea.parametroakGehitu(*$<izenak>2, *$<par_mota>4, *$<mota>5);
	}
	parametro_zerrendaren_bestea|;

sententzia_zerrenda : sententzia_zerrenda sententzia
	{
		 $<jarraituBaldin>$ = new ErrefLista ;
		 $<jarraituBaldin>$->assign($<jarraituBaldin>1->begin(), $<jarraituBaldin>1->end()) ;
	 	 $<jarraituBaldin>$->insert($<jarraituBaldin>$->begin(), $<jarraituBaldin>2->begin(), $<jarraituBaldin>2->end()) ;
		 delete $<jarraituBaldin>1;
		 delete $<jarraituBaldin>2;
	}
	|
	{
		$<jarraituBaldin>$ = new ErrefLista;
	};

sententzia :  TIDENTIFIER TASSIG adierazpena TSEMIC
	{
		kodea.agGehitu(*$<izena>1+" := "+$<adi>3->izena);
		$<jarraituBaldin>$ = new ErrefLista;
	}
	| RIF adierazpena RTHEN M sententzia_zerrenda REND M TSEMIC
	{
		kodea.agOsatu($<adi>2->trueList, $<erref>4);
		kodea.agOsatu($<adi>2->falseList, $<erref>7);
		$<jarraituBaldin>$ = $<jarraituBaldin>5;
	}
	| M RDO sententzia_zerrenda RWHILE M adierazpena REND M TSEMIC
	{
		kodea.agOsatu($<adi>6->trueList, $<erref>1);
		kodea.agOsatu($<adi>6->falseList, $<erref>8);
		kodea.agOsatu(*$<jarraituBaldin>3, $<erref>5);
		$<jarraituBaldin>$ = new ErrefLista;

	}
	| RCONTINUE RIF adierazpena M TSEMIC
	{
		kodea.agOsatu($<adi>3->trueList, $<erref>4);
		$<jarraituBaldin>$ = new ErrefLista;
		$<jarraituBaldin>$->assign($<adi>3->falseList.begin(),$<adi>3->falseList.end()) ;
	        delete $<adi>3;

	}
	| RGET TLPAREN TIDENTIFIER TRPAREN TSEMIC
	{
		kodea.agGehitu("read "+*$<izena>3);
		$<jarraituBaldin>$ = new ErrefLista;
	}
	| RPUT_LINE TLPAREN adierazpena TRPAREN TSEMIC
	{
		kodea.agGehitu("write "+$<adi>3->izena);
		kodea.agGehitu("writeln");
		$<jarraituBaldin>$ = new ErrefLista;
	}
	;

adierazpena : TIDENTIFIER
	{
		 $<adi>$ = new expresionstruct;
		 $<adi>$->izena = *$<izena>1;
		 delete $<izena>1;
	}
        | TINTEGER
	{
		  $<adi>$ = new expresionstruct;
		  $<adi>$->izena = *$<izena>1;
		  delete $<izena>1;
	}
        | TDOUBLE
	{
		 $<adi>$ = new expresionstruct;
		 $<adi>$->izena = *$<izena>1;
		 delete $<izena>1;
	}
	|TLPAREN adierazpena TRPAREN
	{
		$<adi>$ = $<adi>2;
	}

	|adierazpena TADD adierazpena
       {
		$<adi>$ = makearithmetic($<adi>1->izena,*$2,$<adi>3->izena);
      	        delete $<adi>1;
		delete $<izena>2;
      	        delete $<adi>3;
       }

	|adierazpena TSUB adierazpena
       {
		$<adi>$ = makearithmetic($<adi>1->izena,*$2,$<adi>3->izena);
      	        delete $<adi>1;
		delete $<izena>2;
      	        delete $<adi>3;
       }
	|adierazpena TMUL adierazpena
       {
		$<adi>$ = makearithmetic($<adi>1->izena,*$2,$<adi>3->izena);
      	        delete $<adi>1;
		delete $<izena>2;
      	        delete $<adi>3;
       }
	|adierazpena TDIV adierazpena
       {
		$<adi>$ = makearithmetic($<adi>1->izena,*$2,$<adi>3->izena);
      	        delete $<adi>1;
		delete $<izena>2;
      	        delete $<adi>3;
       }
	|adierazpena TCEQ adierazpena
       {
		$<adi>$ = makecomparison($<adi>1->izena,*$2,$<adi>3->izena);
      	        delete $<adi>1;
		delete $<izena>2;
      	        delete $<adi>3;
       }
	|adierazpena TCGT adierazpena
       {
		$<adi>$ = makecomparison($<adi>1->izena,*$2,$<adi>3->izena);
      	        delete $<adi>1;
		delete $<izena>2;
      	        delete $<adi>3;
       }
	|adierazpena TCLT adierazpena
       {
		$<adi>$ = makecomparison($<adi>1->izena,*$2,$<adi>3->izena);
      	        delete $<adi>1;
		delete $<izena>2;
      	        delete $<adi>3;
       }
	|adierazpena TCGE adierazpena
       {
		$<adi>$ = makecomparison($<adi>1->izena,*$2,$<adi>3->izena);
      	        delete $<adi>1;
		delete $<izena>2;
      	        delete $<adi>3;
       }
	|adierazpena TCLE adierazpena
       {
		$<adi>$ = makecomparison($<adi>1->izena,*$2,$<adi>3->izena);
     	        delete $<adi>1;
		delete $<izena>2;
     	        delete $<adi>3;
       }
	|adierazpena TCNE adierazpena
       {
		$<adi>$ = makecomparison($<adi>1->izena,*$2,$<adi>3->izena);
       	        delete $<adi>1;
		delete $<izena>2;
     	        delete $<adi>3;
       };

M : { $<erref>$=kodea.lortuErref(); }
  ;


%%



expresionstruct * makecomparison(std::string &s1, std::string &s2, std::string &s3) {
         expresionstruct *tmp ;
	 tmp = new expresionstruct;
         tmp->trueList.push_back(kodea.lortuErref()) ;
         tmp->falseList.push_back(kodea.lortuErref()+1) ;
         kodea.agGehitu("if " + s1 + " " + s2 + " " + s3 + " goto") ;
         kodea.agGehitu("goto") ;
	 return tmp ;
       }


expresionstruct * makearithmetic(std::string &s1, std::string &s2, std::string &s3) {
         expresionstruct *tmp ;
	 tmp = new expresionstruct;
         tmp->izena = kodea.idBerria() ;
         kodea.agGehitu(tmp->izena + " := " + s1 + " " + s2 + " " + s3) ;
	 return tmp ;
       }
