%option yylineno

%{
 #include <string>
 #include <iostream>
 #include <algorithm>
 #include <vector>
 using namespace std ;
 #include "Lag.h"
 #include "parser.hpp"


 #define TOKEN(t) return t
 #define TOKENST(t) yylval.izena = new std::string(yytext, yyleng) ; return t

 extern "C" int yywrap() { return(1) ; }

%}

%%
programa                TOKEN(RPROGRAM) ;
hasiera                 TOKEN(RBEGIN) ;
amaiera                 TOKEN(REND) ;
prozedura		TOKEN(RPROCEDURE) ;
baldin			TOKEN(RIF) ;
egin 			TOKEN(RDO) ;
bitartean		TOKEN(RWHILE) ;
jarraitu		TOKEN(RCONTINUE) ;
irakurri 		TOKEN(RGET) ;
idatzi_lerroa 		TOKEN(RPUT_LINE) ;
in 			TOKEN(RIN) ;
out			TOKEN(ROUT) ;
orduan			TOKEN(RTHEN) ;
osoko			TOKEN(RINTEGER);
erreal			TOKEN(RFLOAT);
aldagaiak		TOKEN(RVAR);

">="			TOKENST(TCGE);
"<="			TOKENST(TCLE);
"<"			TOKENST(TCLT);
">"			TOKENST(TCGT);
"/="			TOKENST(TCNE);
"=="			TOKENST(TCEQ);
"*"                     TOKENST(TMUL);
"/"			TOKENST(TDIV);
"+"			TOKENST(TADD);
"-"			TOKENST(TSUB);
"="                     TOKENST(TASSIG);
";"                     TOKEN(TSEMIC);
","			TOKEN(TCOMMA);
"("			TOKEN(TLPAREN);
")"			TOKEN(TRPAREN);
":"			TOKEN(TCOLON);

[ \t\n]                 ;
\(\*([^*]|\*+[^*)])*\*+\) ;

[a-zA-Z][a-zA-Z0-9]*(_[a-zA-Z0-9]+)*     TOKENST(TIDENTIFIER) ;
[0-9]+\.[0-9]+([Ee][-+]?[0-9]+)?          TOKENST(TDOUBLE);
[0-9]+                  TOKENST(TINTEGER);

.                       { cout << "Token ezezaguna: " << yytext << endl; yyterminate();}
%%
