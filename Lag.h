#ifndef LAG_H_
#define LAG_H_

#include <string>
#include <set>
#include <vector>
#include <list>

typedef std::list<std::string> IdLista;
typedef std::list<int> ErrefLista;

struct expresionstruct {
  std::string izena ;
  ErrefLista trueList ;
  ErrefLista falseList ;
};

#define SINTEGER "integer"
#define SFLOAT "float"

#endif /* LAG_H_ */
