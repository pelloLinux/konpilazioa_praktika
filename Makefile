CFLAGS=-Wall
SOURCES=parser.cpp main.cpp tokens.cpp Kodea.cpp

all: parser proba

clean:
	rm parser.cpp parser.hpp parser tokens.cpp *~

parser.cpp: parser.y
	bison -d -o $@ $^

parser.hpp: parser.cpp

tokens.cpp: tokens.l parser.hpp
	flex -o $@ $<

parser: $(SOURCES) Kodea.h Lag.h
	g++ $(CFLAGS) -o $@ $(SOURCES)

proba:  parser probaona1.dat
	echo "probaona1.dat"
	./parser <probaona1.dat
	echo "probaona2.dat"
	./parser <probaona2.dat
	echo "probaona3.dat"
	./parser <probaona3.dat
	echo "probatxar1.dat"
	./parser <probatxar1.dat
	echo "probatxar2.dat"
	./parser <probatxar2.dat
	echo "probatxar3.dat"
	./parser <probatxar3.dat
	echo "probatxar4.dat"
	./parser <probatxar4.dat
	echo "probatxar5.dat"
	./parser <probatxar5.dat
